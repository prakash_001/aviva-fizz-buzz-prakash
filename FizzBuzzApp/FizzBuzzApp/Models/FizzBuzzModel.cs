﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzzApp.Models
{
    public class FizzBuzzModel
    {

        [Required]
        [Display(Name = "Input Value")]
        //For range validation
        [Range(1, 1000)]
        public  int InputValue { get; set; }
        public List<string> lstValue { get; set; }
    
    }
}
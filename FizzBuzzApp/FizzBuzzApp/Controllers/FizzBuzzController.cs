﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzzApp.Models;
using FizzBuzzApp.Business;


namespace FizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        //
        // GET: /FizzBuzz/

        public ActionResult Index()
        {
            FizzBuzzModel model = new FizzBuzzModel();
            return View(model);
        }

        public ActionResult ShowResult(FizzBuzzModel model)
        {
            if (ModelState.IsValid)
            {
                int txtval;
                txtval = model.InputValue;
                List<string> strValue = new List<string>();
                //Calling Business Logic to execute the values
                Logic retvalue = new Logic();
                strValue = retvalue.RetuenValue(txtval);
                model.lstValue = strValue;

            }
            return View("index", model);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzApp.Business
{
    public class Logic
    {
        public List<string> RetuenValue(int val)
        {
            List<string> lst = new List<string>();
            //Collection<T> collection = new Collection<T>();

            for (int i = 1; i <= val; i++)
            {
                string finalVal = string.Empty;

                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                {
                    finalVal += (i % 3) == 0 ? "Wizz" : "";
                    finalVal += (i % 5) == 0 ? "Wuzz" : "";

                }
                else
                {
                    finalVal += (i % 3) == 0 ? "Fizz" : "";
                    finalVal += (i % 5) == 0 ? "Buzz" : "";
                }

                finalVal += finalVal == "" ? Convert.ToString(i).ToString() : "";
                lst.Add(finalVal);
            }

            return lst;

        }

    }
}
